FROM ruby:3.1-bookworm

# exclude unnecessary files from being installed

RUN echo "path-exclude=/usr/share/locale/*\npath-exclude=/usr/share/man/*\npath-exclude=/usr/share/doc/*\npath-exclude=/usr/share/fonts/*" > /etc/dpkg/dpkg.cfg.d/nodoc

# installed required debian packages

RUN apt-get update && \
  apt-get install -y \
    # required helper applications
    nodejs wkhtmltopdf sqlite3 \
    # libraries needed to build gems
    ruby-dev build-essential libsqlite3-dev libsodium23 \
    libyaml-dev libmariadb-dev libvips42 && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /usr/share/doc && \
  rm -rf /usr/share/man && \
  rm -rf /usr/share/fonts

