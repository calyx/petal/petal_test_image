# petal_test_image

This is a base docker image used for running automated gitlab-runner tests for petal.

## Usage

Build the new image:

    docker build -t petal_test -t registry.0xacab.org/calyx/infra/registry .

Upload to registry:

    docker login registry.0xacab.org
    docker push registry.0xacab.org/calyx/container/registry

Use in .gitlab-ci.yml:

    image: "registry.0xacab.org/calyx/container/registry/petal_test:latest"
